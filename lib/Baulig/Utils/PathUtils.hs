{-|
Module:       Baulig.Utils.PathUtils
Description:  Path related utilities.
Copyright:    (c) Martin Baulig, 2022-2023
License:      BSD 3-clause
Stability:    stable
Portability:  POSIX

Just a few utility functions.
-}
module Baulig.Utils.PathUtils
    ( stripPathPrefix
    , uriToText
    , validateRelativePath
    ) where

import           Baulig.Prelude

import qualified RIO.FilePath          as F
import qualified RIO.Text              as T

import           Network.URI

import           Text.Regex.PCRE.Light

-----------------------------------------------------------------------------
-- PathUtils
-----------------------------------------------------------------------------
{-|
  Validate whether the provided 'FilePath' is a well-formed relative path name.

  This is a pure function, which uses a regular expression match for it's validation.
-}
validateRelativePath :: FilePath -> Bool
validateRelativePath (encodeUtf8 . str2txt -> input) =
    let regex = compile "^(([A-Za-z0-9\\.][A-Za-z0-9_-]*/)*)([A-Za-z0-9_-]+)\\.([A-Za-z0-9]+)?$"
                        []
    in
        isJust $ match regex input []

{-|
  Strips a prefix from a path.

  This function wraps 'T.stripPrefix' in a way suitable for path names: both the
  prefix and the base path are first normalized.
-}
stripPathPrefix :: Text -> Text -> Maybe Text
stripPathPrefix prefix path' =
    let first' =
            T.pack $ F.addTrailingPathSeparator $ F.normalise $ T.unpack prefix
        second' =
            T.pack $ F.dropTrailingPathSeparator $ F.normalise $ T.unpack path'
    in
        T.stripPrefix first' second'

-- | Convert 'URI' to 'Text'.
uriToText :: URI -> Text
uriToText uri = T.pack $ uriToString id uri ""
