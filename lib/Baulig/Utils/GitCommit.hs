{-|
Module:       Baulig.Utils.GitCommit
Description:  Git Commit helper functions.
Copyright:    (c) Martin Baulig, 2022-2023
License:      BSD 3-clause
Stability:    unstable
Portability:  POSIX

This is mostly a wrapper layer around the 'Git' module, which is unfortunately
very little documented and also a bit difficult to get used to.
-}
module Baulig.Utils.GitCommit
    ( CommitAuthor(..)
    , CommitMessage(..)
    , GitCommit(..)
    , fromGitLibCommit
    , module Baulig.Utils.CommitHash
    ) where

import           Baulig.Prelude

import           RIO.Time

import qualified Git                     as G

import           Baulig.Utils.CommitHash

------------------------------------------------------------------------------
--- CommitAuthor
------------------------------------------------------------------------------
-- | The commit author.
data CommitAuthor =
    CommitAuthor { commitAuthorName :: !Text, commitAuthorEmail :: !Text }
    deriving stock (Eq, Show)

instance Display CommitAuthor where
    display this =
        display $ this <:> commitAuthorName this <+> commitAuthorEmail this

------------------------------------------------------------------------------
--- CommitMessage
------------------------------------------------------------------------------
-- | The commit message.
newtype CommitMessage = CommitMessage { unCommitMessage :: Text }
    deriving stock (Eq, Show)

instance Display CommitMessage where
    display this = display $ this <:> unCommitMessage this

------------------------------------------------------------------------------
--- GitCommit
------------------------------------------------------------------------------
{-|
  A single Git commit, consisting of it's 'CommitHash', 'CommitAuthor' and 'CommitMessage'.

  Unlike 'G.Commit', we do not distinguish between author and committer.  From my
  personal experience, it is quite rare for those to be different.

  This also allows us to only keep track of a single 'gitCommitTime'.
-}
data GitCommit = GitCommit { gitCommitHash    :: !CommitHash
                           , gitCommitAuthor  :: !CommitAuthor
                           , gitCommitMessage :: !CommitMessage
                           , gitCommitTime    :: !LocalTime
                           }
    deriving stock (Eq, Show)

instance Display GitCommit where
    display this =
        display $ this <:> gitCommitHash this <+> gitCommitAuthor this
        <+> gitCommitMessage this <+!> gitCommitTime this

------------------------------------------------------------------------------
--- GitCommit
------------------------------------------------------------------------------
-- | Convert a 'G.Commit' from @gitlib@ to a 'GitCommit'.
fromGitLibCommit :: G.IsOid (G.Oid r) => G.Commit r -> GitCommit
fromGitLibCommit commit =
    GitCommit { gitCommitHash    =
                    commitHash $ G.renderObjOid $ G.commitOid commit
              , gitCommitAuthor  = CommitAuthor (G.signatureName author)
                                                (G.signatureEmail author)
              , gitCommitMessage = CommitMessage $ G.commitLog commit
              , gitCommitTime    = zonedTimeToLocalTime $ G.signatureWhen author
              }
  where
    author = G.commitAuthor commit
