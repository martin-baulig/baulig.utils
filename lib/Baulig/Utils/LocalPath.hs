{-|
Module:       Baulig.Utils.LocalPath
Description:  Distinction between absolute and relative paths at the type level.
Copyright:    (c) Martin Baulig, 2022-2023
License:      BSD 3-clause
Stability:    stable
Portability:  POSIX

When dealing with file system paths, it is often desirable to know whether some
path is absolute or relative.

This module provides an easy way to declare this on the type level, thus allowing
a function to require an absolute (or relative) path in it's type signature.
-}
module Baulig.Utils.LocalPath
    ( AbsoluteLocalPath
    , RelativeLocalPath
    , HasRelativePath(..)
    , HasAbsolutePath(..)
    , relativeLocalPath
    , absoluteLocalPath
    , absoluteRootPath
    ) where

import           Baulig.Prelude

import qualified RIO.FilePath           as F
import qualified RIO.Text               as T

import           Baulig.Utils.PathUtils

------------------------------------------------------------------------------
--- HasRelativePath
------------------------------------------------------------------------------
infixr 5 </>

{-|
  Abstraction layer resembling things that can be relative file paths.
-}
class HasRelativePath x where
    {-|
      The corresponding type that represents a relative file path.

      This is the same as the current instance type except for 'AbsoluteLocalPath',
      in which case it will point to 'RelativeLocalPath'.
    -}
    type RelativePathType x

    -- | Infix version of 'combine'.
    (</>) :: HasRelativePath y => x -> y -> x
    (</>) = combine

    {-|
      Combines an absolute or relative path with an additional relative segment.

      This internall calls 'asRelativePath', so it you are attempting to append an
      'AbsoluteLocalPath', only it's relative part will be used.
    -}
    combine :: HasRelativePath y => x -> y -> x

    {-|
      Turns this into a relative path.

      This is equivalent to 'id'  except for 'AbsoluteLocalPath', in which case it
      will return it's relative portion (or the empty string if it doesn't have any).
    -}
    asRelativePath :: x -> RelativePathType x

    -- | Same as 'asRelativePath', but returning a 'FilePath'.
    relativeFilePath :: x -> FilePath

    {-|
      Takes a prefix and a path and tries to strip that prefix from the path.

      Returns 'Nothing' if the given path does not start with the given prefix,
      otherwise the stripped relative path.
    -}
    stripPrefix :: RelativePathType x -> x -> Maybe (RelativePathType x)

    {-|
      Convenience helper function to check whether the given path starts with
      a certain prefix.
    -}
    hasPrefix :: RelativePathType x -> x -> Bool
    hasPrefix prefix = isJust . stripPrefix prefix

    {-|
      Sets the file name extension to the one provided.

      This is essentially a wrapper around 'F.replaceExtension', adjusted for the
      types defined in this module.
    -}
    replaceExtension :: x -> Text -> x

instance HasRelativePath FilePath where
    type RelativePathType FilePath = FilePath

    combine path (relativeFilePath -> append) = F.combine path append

    asRelativePath = id

    relativeFilePath = id

    stripPrefix prefix path = T.unpack
        <$> stripPathPrefix (T.pack prefix) (T.pack path)

    replaceExtension path (T.unpack -> ext) = F.replaceExtension path ext

instance HasRelativePath Text where
    type RelativePathType Text = Text

    combine (T.unpack -> path) (relativeFilePath -> append) =
        T.pack $ F.combine path append

    asRelativePath = id

    relativeFilePath = T.unpack

    stripPrefix = stripPathPrefix

    replaceExtension (T.unpack -> path) (T.unpack -> ext) =
        T.pack $ F.replaceExtension path ext

------------------------------------------------------------------------------
--- HasAbsolutePath
------------------------------------------------------------------------------
{- | Abstraction layer for absolute file paths. -}
class HasRelativePath x => HasAbsolutePath x where
    {-|
      Combines both the absolute root and the relative part of an 'AbsoluteLocalPath',
      joins them both together and returns the result as a 'FilePath'.
    -}
    absoluteFilePath :: x -> FilePath

    {-|
      Returns a new 'AbsoluteLocalPath' containing only the root stem without the
      relative path.
    -}
    absolutePathRoot :: x -> x

    {-|
      Takes an absolute path and anything that can resemble a relative path,
      joins the two together and returns the result as an absolute path.
    -}
    prefixAbsolutePath :: HasRelativePath y => x -> y -> x

------------------------------------------------------------------------------
--- AbsoluteLocalPath
------------------------------------------------------------------------------
{-|
An absolute local path name, consisting of a "root" or prefix and an optional
path relative to it.

This is an opaque type, use 'absoluteLocalPath' to construct an instance of it.

To denote the root of a directory tree:

@
absoluteLocalPath "//Workspace//baulig.utils" Nothing
@

To denote a file inside of that directory tree:

@
absoluteLocalPath "//Workspace//baulig.utils" $ Just "stack.yaml"
@

Can be combined with a 'RelativeLocalPath', for instace:

@
let root      = absoluteLocalPath "//Workspace//baulig.utils" Nothing
let stackYaml = relativeLocalPath "stack.yaml"

let absolute  = root \</\> stackYaml
@

-}
data AbsoluteLocalPath =
    AbsoluteLocalPath { alpRoot :: !Text, alpRelative :: !(Maybe Text) }
    deriving stock (Eq, Show, Generic)

instance Binary AbsoluteLocalPath

instance Hashable AbsoluteLocalPath

instance Display AbsoluteLocalPath where
    display path = display $ path <:> alpRoot path <+> alpRelative path

instance HasRelativePath AbsoluteLocalPath where
    type RelativePathType AbsoluteLocalPath = RelativeLocalPath

    combine absolute (relativeFilePath -> append) =
        let newRelative = case alpRelative absolute of
                Just (T.unpack -> relative) -> F.combine relative append
                Nothing -> append
        in
            absolute { alpRelative = Just $ T.pack newRelative }

    relativeFilePath absolute = maybe "" T.unpack (alpRelative absolute)

    asRelativePath absolute = maybe "" RelativeLocalPath (alpRelative absolute)

    stripPrefix prefix path =
        stripPrefix (asRelativePath prefix) (asRelativePath path)

    replaceExtension path (T.unpack -> ext) =
        path { alpRelative = alpRelative path <&> replace }
      where
        replace relative = T.pack $ F.replaceExtensions (T.unpack relative) ext

instance HasAbsolutePath AbsoluteLocalPath where
    absoluteFilePath (AbsoluteLocalPath (T.unpack -> root) maybeRelative) =
        case maybeRelative of
            Just relative -> F.combine root $ T.unpack relative
            Nothing -> root

    absolutePathRoot (AbsoluteLocalPath root _) =
        AbsoluteLocalPath root Nothing

    prefixAbsolutePath absolute (relativeFilePath -> append) =
        let newRelative = case alpRelative absolute of
                Just relative -> F.combine append $ T.unpack relative
                Nothing -> append
        in
            absolute { alpRelative = Just $ T.pack newRelative }

{-|
  Takes an absolute path root and an optional relative part and constructs
  an instance of 'AbsoluteLocalPath' with it.
-}
absoluteLocalPath :: Text -> Maybe Text -> AbsoluteLocalPath
absoluteLocalPath = AbsoluteLocalPath

-- | Convenience function to construct an 'AbsoluteLocalPath' from a 'FilePath'.
absoluteRootPath :: FilePath -> AbsoluteLocalPath
absoluteRootPath = flip absoluteLocalPath Nothing . T.pack

------------------------------------------------------------------------------
--- RelativeLocalPath
------------------------------------------------------------------------------
{-|
  A relative local path.

  This is an opaque type, use 'relativeLocalPath' to construct an instance of it.
-}
newtype RelativeLocalPath = RelativeLocalPath { unRelativeLocalPath :: Text }
    deriving stock (Eq, Show, Generic)

instance Binary RelativeLocalPath

instance Hashable RelativeLocalPath

instance Display RelativeLocalPath where
    display path = display $ path <:> unRelativeLocalPath path

instance IsString RelativeLocalPath where
    fromString = RelativeLocalPath . T.pack

instance HasRelativePath RelativeLocalPath where
    type RelativePathType RelativeLocalPath = RelativeLocalPath

    combine (T.unpack . unRelativeLocalPath -> oldRelative)
            (relativeFilePath -> append) =
        RelativeLocalPath $ T.pack $ F.combine oldRelative append

    relativeFilePath = T.unpack . unRelativeLocalPath

    asRelativePath = id

    stripPrefix prefix path = RelativeLocalPath
        <$> stripPathPrefix (unRelativeLocalPath prefix)
                            (unRelativeLocalPath path)

    replaceExtension old ext = RelativeLocalPath $ T.pack
        $ F.replaceExtensions (T.unpack $ unRelativeLocalPath old)
                              (T.unpack ext)

{-|
  Takes anything that can represent a relative file path and constructs
  an instance of 'RelativeLocalPath' with it.

  You can pass it a 'FilePath', 'Text', 'AbsoluteLocalPath' or
  'RelativeLocalPath'.
-}
relativeLocalPath :: HasRelativePath x => x -> RelativeLocalPath
relativeLocalPath = RelativeLocalPath . T.pack . relativeFilePath
