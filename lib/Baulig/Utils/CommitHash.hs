{-|
Module:       Baulig.Utils.CommitHash
Description:  Git Commit SHA.
Copyright:    (c) Martin Baulig, 2022-2023
License:      BSD 3-clause
Stability:    unstable
Portability:  POSIX

Represents a GIT Commit SHA in a type-safe manner.
-}
module Baulig.Utils.CommitHash
    ( CommitHash(unCommitHash) -- | We do not export the constructor to enforce validation via 'commitHash'.
    , commitHash
    , validateCommitHash
    ) where

import           Baulig.Prelude

import           Text.Regex.PCRE.Light

------------------------------------------------------------------------------
--- CommitHash
------------------------------------------------------------------------------
-- | A Git commit hash.
newtype CommitHash = CommitHash { unCommitHash :: Text }
    deriving stock (Eq
                  , Show
                  , Ord)

instance Display CommitHash where
    display this = display $ this <:> unCommitHash this

{-|
  Validate whether the provided text is a well-formed Git commit SHA.
-}
validateCommitHash :: Text -> Bool
validateCommitHash (encodeUtf8 -> input) =
    let regex = compile "^[A-Za-z0-9\\.]{40}$" []
    in
        isJust $ match regex input []

{-|
  Constructs a 'GitCommit' from a GIT commit SHA; guarding against invalid input.
-}
commitHash :: Text -> CommitHash
commitHash sha
    | validateCommitHash sha = CommitHash sha
commitHash invalid = error $ "Invalid git commit SHA: " <> txt2str invalid

