{-|
Module:       Baulig.Utils.Bottleneck
Description:  Provides "bottleneck" synchronization primitives.
Copyright:    (c) Martin Baulig, 2022-2023
License:      BSD 3-clause
Stability:    stable
Portability:  POSIX

Provides thread-safe access to a function by wrapping it with a @Bottleneck@.
-}
module Baulig.Utils.Bottleneck
    ( Bottleneck
    , newBottleneck
    , runBottleneck
    , withBottleneckLogger
    ) where

import           Baulig.Prelude

------------------------------------------------------------------------------
--- Bottleneck Logger
------------------------------------------------------------------------------
newtype LogCallback = LogCallback (IO ())

{-|
  Wraps a 'LogFunc' with a @Bottleneck@, to allow thread-safe access to RIO's
  logging functions.
-}
withBottleneckLogger
    :: MonadUnliftIO m => LogOptions -> (LogFunc -> m x) -> m x
withBottleneckLogger options func = do
    baton <- newEmptyMVar
    withLogFunc options $ \rawLogFunc -> do
        let logFunc = mkLogFunc $ writeLog rawLogFunc baton
        result <- race (func logFunc) $ liftIO $ loop baton
        case result of
            Left retval -> pure retval
            Right void' -> absurd void'
  where
    loop :: MVar LogCallback -> IO Void
    loop baton = do
        LogCallback callback <- takeMVar baton
        callback
        loop baton

    writeLog :: LogFunc
             -> MVar LogCallback
             -> CallStack
             -> LogSource
             -> LogLevel
             -> Utf8Builder
             -> IO ()
    writeLog rawLogFunc baton _ source level builder = putMVar baton
        $ LogCallback $ runRIO rawLogFunc $ logGeneric source level builder

------------------------------------------------------------------------------
--- Bottleneck
------------------------------------------------------------------------------
{-|
  Opaque data type; call 'newBottleneck' to create a new instance of this type.

  Pass this to 'runBottleneck' together with an arbitrary function.
-}
newtype Bottleneck = Bottleneck QSem

newBottleneck :: MonadIO m => m Bottleneck
newBottleneck = Bottleneck <$> newQSem 1

{-|
  This function is intended for a multi-threaded program to guard a piece of code
  (that is not thread-safe) against any concurrent invocations - by ensuring that
  only a single thread at a time can access the provided @Bottleneck@.

  It is essentially the Haskell version of a POSIX mutex - or a @lock@ statement
  from other programming languages.
-}
runBottleneck :: HasLogFunc env => RIO env x -> Bottleneck -> RIO env x
runBottleneck func (Bottleneck sem) =
    bracket_ (logDebug "BOTTLENECK" >> waitQSem sem)
             (logDebug "BOTTLENECK DONE" >> signalQSem sem)
             (logDebug "BOTTLENECK INNER" >> func)
