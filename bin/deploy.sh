#!/bin/sh

stack build --haddock --no-haddock-deps
mkdir -p public
cp -a `stack path --local-install-root`/doc/* public
